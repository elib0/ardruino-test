const SERVER_IP = require('ip').address();
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// path al app
const rootPath = path.normalize(__dirname);

// Archivos estaticos
app.use(express.static(`${rootPath}/public`));
app.set('views', `${rootPath}/views`);

// Rutas
app.get('/', function (req,res) {
  res.render('led.html');
});

http.listen(3000, () => {
  console.log('Escuchando en el puerto *:3000');
});
