// ref: https://boneskull.com/how-to-use-an-esp8266-with-johnny-five/
const {
  EtherPortClient
} = require('etherport-client');
const { Board, Led, Pin } = require('johnny-five');
const five = require('johnny-five');

const board = new Board({
  port: new EtherPortClient({
    host: '192.168.0.175',
    port: 3030
  }),
  repl: false
});

const SERVER_PORT = 3000;
const SERVER_IP = require('ip').address();
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// path al app
const path = require('path');
const rootPath = path.normalize(__dirname);

// Archivos estaticos
app.use(express.static(`${rootPath}/public`));
// app.set('views', `${rootPath}/views`);

// Rutas
app.get('/', function (req,res) {
  res.sendFile(`${rootPath}/public/views/led.html`);
});

// Arrancar server
http.listen(SERVER_PORT, () => {
  console.log(`Escuchando en el puerto ${SERVER_IP}:${SERVER_PORT}`);
});

const LED_PIN = 2;

board.on('ready', () => {
  board.pinMode(LED_PIN, Pin.OUTPUT);
  // the Led class was acting hinky, so just using Pin here
  const pin = new Pin(LED_PIN);
  let interval = null;
  let value = 0;

  io.on('connection', function (socket) {

    socket.on('apagar', () => {
      pin.low();
      if (interval) {
        clearInterval(interval);
      }
      io.emit('apagado', false);
    });

    socket.on('prender', () => {
      pin.high();
      io.emit('prendido', true);
    });

    socket.on('titilar', (duration) => {
      interval = setInterval(() => {
        if (value) {
          pin.high();
          value = 0;
        } else {
          pin.low();
          value = 1;
        }
      }, parseInt(duration) / 2);
    });
  })
});
