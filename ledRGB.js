const { Board, Led } = require('johnny-five');

const board = new Board({port: 'COM3'});

const SERVER_PORT = 3000;
const SERVER_IP = require('ip').address();
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// path al app
const path = require('path');
const rootPath = path.normalize(__dirname);

// Archivos estaticos
app.use(express.static(`${rootPath}/public`));
// app.set('views', `${rootPath}/views`);

// Rutas
app.get('/', function (req,res) {
  res.sendFile(`${rootPath}/public/views/ledRGB.html`);
});

// Arrancar server
http.listen(SERVER_PORT, () => {
  console.log(`Escuchando en el puerto ${SERVER_IP}:${SERVER_PORT}`);
});


board.on('ready', () => {
  const rgb = new Led.RGB([5, 6, 3]);
  // let index = 0;
  // const rainbow = ["FF0000", "FF7F00", "FFFF00", "00FF00", "0000FF", "4B0082", "8F00FF"];
  // board.loop(1000, () => {
  //   rgb.color(rainbow[index++]);
  //   if (index === rainbow.length) {
  //     index = 0;
  //   }
  // });

  io.on('connection', function (socket) {

    socket.on('apagar', () => {
      rgb.stop().off();
      // io.emit('apagado', rgb);
    });

    socket.on('prender', (color) => {
      color = color || 'FF0000';
      if (color !== '000000') {
        rgb.color(color).on();
      } else {
        rgb.off();
      }
      // io.emit('prendido', rgb);
    });

  })
});
