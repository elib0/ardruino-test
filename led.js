const { Board, Led } = require('johnny-five');

const board = new Board({port: 'COM3'});

const SERVER_PORT = 3000;
const SERVER_IP = require('ip').address();
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// path al app
const path = require('path');
const rootPath = path.normalize(__dirname);

// Archivos estaticos
app.use(express.static(`${rootPath}/public`));
// app.set('views', `${rootPath}/views`);

// Rutas
app.get('/', function (req,res) {
  res.sendFile(`${rootPath}/public/views/led.html`);
});

// Arrancar server
http.listen(SERVER_PORT, () => {
  console.log(`Escuchando en el puerto ${SERVER_IP}:${SERVER_PORT}`);
});


board.on('ready', () => {
  const led = new Led(13);
  io.on('connection', function (socket) {

    socket.on('apagar', () => {
      led.stop().off();
      // io.emit('apagado', led);
    });

    socket.on('prender', () => {
      led.on();
      // io.emit('prendido', led);
    });

    socket.on('titilar', (duration) => {
      led.blink(duration);
      // io.emit('prendido', led);
    });
  })
  // led.stop().on();

  // board.repl.inject({
  //   led
  // });

  // led.pulse({
  //   easing: "linear",
  //   duration: 3000,
  //   cuePoints: [0, 0.2, 0.4, 0.6, 0.8, 1],
  //   keyFrames: [0, 10, 0, 50, 0, 255],
  //   onstop() {
  //     console.log("Animation stopped");
  //   }
  // });
  //
  // board.repl.inject({
  //   led
  // });
  //
  // board.wait(12000, () => {
  //
  //   // stop() terminates the interval
  //   // off() shuts the led off
  //   led.stop().off();
  // });
});
